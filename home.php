<html>
<head>
	<meta charset="UTF-8">
	<title>Controle de Finanças</title>
	<link rel="stylesheet" href="style/index_style.css">
	<link rel="stylesheet" type="text/css" href="login.css">
</head>

<body>
	
	<div id="titulo">
		<p id="header">Controle de Finanças</p>
		
	</div>

	<header>
		
		<div class="container" id="dois">
			<a href="despesa.php" class="icon">
			<img class="icon" src="pictures/icon2.png">
			</a>
			<p class="texto">Nova Despesa</p>
			
		</div>

		<div class="container" id="tres">
		<a href="lancamentos.php" class="icon">
			<img class="icon" src="pictures/icon3.png">
			</a>
			<p class="texto">Despesas Lançadas</p>
		</div>

		<div class="container" id="quatro">
		<a href="carteira.php" class="icon">
			<img class="icon" src="pictures/icon4.png">
			</a>
			<p class="texto">Carteira</p>
		</div>

		<div class="container" id="cinco">
		<a href="saldo.php" class="icon">
			<img class="icon" src="pictures/icon5.png">
			</a>
			<p class="texto">Saldo</p>
		</div>

		<div class="container" id="seis">
		<a href="logoff.php" class="icon">
			<img class="icon" src="pictures/icon6.png">
			</a>
			<p class="texto">Sair</p>
		</div>

	</header>
</body>


